# -*- coding: utf-8 -*-

#python imports
import re
import urllib2
from datetime import date

#scrapy imports
from scrapy.contrib.spiders import CrawlSpider
from scrapy.contrib.spiders import Rule 
from scrapy.http import Request
from scrapy.selector import HtmlXPathSelector 
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor 

#datacollector imports
from datacollector.core.utils import *
from datacollector.core.dataproc import *
# TODO fix imports
# from quoka.database import anbieter_table
from quoka.items import QuokaItem

class QuokaSpider(CrawlSpider):
    name = 'quoka'
    allowed_domains = ['quoke.de']
    start_urls = (
            'quoke.de', 
            )
