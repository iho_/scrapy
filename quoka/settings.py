ITEM_PIPELINES = {'quoka.pipelines.QuokaPipeline': 100}

# DB password
DB_USER = None
DB_PASSWD = None
DB_HOST = None
DB_NAME = None

SPIDER_MODULES = ['quoka.spiders']
