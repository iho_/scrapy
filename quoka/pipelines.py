# -*- coding: utf-8 -*-

#python imports
import re
from datetime import date

#scrapy imports
from sqlalchemy.sql.expression import text
from scrapy.exceptions import DropItem
from .utils import *
from .dataproc import *
from .database import *

SessionMaker = connect_to_db()

class QuokaPipeline(object):

    def process_item(self, item, spider):
        session = SessionMaker()
        obj = QuokaData(**dict(item))
        session.add(obj)
        session.commit()
        session.remove()
        return item
        
    
