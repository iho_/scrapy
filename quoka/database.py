# -*- coding: utf-8 -*-
"""
Object representation info about spiders in the database.
The base interfaces to work with the database.
"""

from subprocess import Popen, PIPE
from sqlalchemy import Column, Integer, String, ForeignKey, Boolean, DateTime, Text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import datacollector.core.settings as conf
# TODO from datacollector.core.collector import check_loopback
Base = declarative_base()

def check_loopback(value):
    return value

def connect_to_db():
    """Connect to database specified in config.py
    
    Returns: 
        Sqlalchemy Session object.
        http://docs.sqlalchemy.org/en/rel_0_8/orm/session.html
    """
    db_url = 'mysql://%s:%s@%s/%s?charset=utf8&use_unicode=1'\
             % (conf.DB_USER, conf.DB_PASSWD, check_loopback(conf.DB_HOST), conf.DB_NAME)
    engine = create_engine(db_url, convert_unicode=False, echo=False)
    DBSession = sessionmaker()
    DBSession.configure(bind=engine)
    Base.metadata.create_all(engine)
    
    return DBSession()

def sql_from_file(path):
    process = Popen('mysql -u %s -h %s --password=%s %s' % (conf.DB_USER, conf.DB_HOST, conf.DB_PASSWD, conf.DB_NAME),
                    stdout=PIPE, stdin=PIPE, shell=True)
    output = process.communicate('source ' + path)[0]
    
class Site(Base):
    """Base object - site.
    Each spider attached to the site obj.
    """
    __tablename__ = 'sites'
    id = Column(Integer, primary_key=True)
    prefix = Column(String(10), nullable=False) 
    domain = Column(String(50), nullable=False)
    spiders = relationship("Spider", backref="site")
    
    def __init__(self, prefix, domain):
        self.prefix = prefix
        self.domain = domain

class Spider(Base):
    __tablename__ = 'spiders'
    id = Column(Integer, primary_key=True)
    site_id = Column(Integer, ForeignKey('sites.id'), nullable=False)
    name = Column(String(50), nullable=False)
    start_urls = relationship("StartUrl", backref="spider")
    
    def __init__(self, site_id, name):
        self.site_id = site_id
        self.name = name
    
class StartUrl(Base):
    """Class describing the information about links that spider pass in.
    
    Attributes:
        spider_id: start url belong to some spider obj.
        name: category name.
        url: start url
        scrapes_count: the number of passes this url
        items_count: the number of entities on this link
    """
    __tablename__ = 'start_urls'
    id = Column(Integer, primary_key=True)
    spider_id = Column(Integer, ForeignKey('spiders.id'), nullable=False)
    name = Column(String(50))
    url = Column(String(255), nullable=False)
    scrapes_count = Column(Integer, default=0)
    items_count = Column(Integer)    
    
    def __init__(self, spider_id, name, url, scrapes_count=0, items_count=0):
        self.spider_id = spider_id
        self.name = name
        self.url = url
        self.scrapes_count = scrapes_count
        self.items_count = items_count
        
class Proxy(Base):
    """Class contains information about using proxies
    
    Attributes:
        proxy: proxy url.
        collector: server uses this proxy.
        is_valid: work status. 
    """
    __tablename__ = 'proxies'
    id = Column(Integer, primary_key=True)
    proxy = Column(String(255), nullable=False)
    created = Column(DateTime, nullable=False)
    is_valid = Column(Boolean, default=True)
    
    def __init__(self, proxy, created, is_valid=True):
        self.proxy = proxy
        self.created = created
        self.is_valid = is_valid

class QuokaData(Base):
    __tablename__ = 'quoka_data'
    id = Column(Integer, primary_key=True)
    url = Column(String(255), nullable=False)

    Immobilientyp = Column(String(255), nullable=False)

    Erstellungsdatum = Column(String(255), nullable=False)
    Telefon = Column(String(255), nullable=False)
    Ueberschrit = Column(String(255), nullable=False)
    Preis = Column(String(255), nullable=False)
    PLZ = Column(String(255), nullable=False)
    Stadt = Column(String(255), nullable=False)
    Beschreibung = Column(Text, nullable=False)
