# -*- coding: utf-8 -*-
"""
Simple set of string processors
From datacollector.core.dataproc
"""

import re

def s_format(s, if_none=None):
    """Converts the string to a readable format
    """
    if not s:
        return if_none
    s = re.sub('\s+', ' ', s)
    s = re.sub('<[^<]+?>', '  ', s)
    s = re.sub('\s{2,}', '\n', s)
    s = re.sub('^\s+', '', s)

    return s

def clean_space(s, if_none=''):
    """Remove extra space.
    """
    if not s:
        return if_none
    s = re.sub('(<[^<]+?>|&.{0,}?;)', '', re.sub('[\r\n\t]*\s{2,}', ' ', s)).strip()
    s = u" ".join(s.split())

    return s or if_none
    
def n_format(s, integer=False, if_none=''):
    """Convert string to a number format
    """
    if not s:
        return if_none
    try:    
        s = re.sub('[^\d\.,]', '', s)
        s = re.search('\d+(\.\d+)*(,\d+)*', s).group()
    except AttributeError:
        return if_none
    
    s = re.sub('\.', '', s)

    if integer:
        return int(s)
        
    return s or if_none
