# -*- coding: utf-8 -*-

"""
From datacollector.core.utils
"""

import re, random
import smtplib
from email.mime.text import MIMEText
import datacollector.core.settings as conf

def set_spider_settings(spider_name, settings):
    """Override the settings for an individual spider
    
    Args:
        spider_name: the name of the spider for which the settings are overwritten
        settings: scrapy Settings object
            http://doc.scrapy.org/en/0.16/topics/api.html#scrapy.settings.Settings
    """
    per_spider = settings.get('PER_SPIDER_SETTINGS').get(spider_name)
    
    if per_spider:
        for k, v in per_spider.items():
            settings.overrides[k] = v            
    
def send_warning(spider_name, field):
    msg = MIMEText("You recieved this warning because of suspected poor data quality on %s field. Please check it!" % field)
    msg['Subject'] = 'Tests %s' % spider_name
    msg['From'] = 'Datacollector'
    msg['To'] = ', '.join(conf.DEST_EMAILS)
    s = smtplib.SMTP(conf.SMTP_SERVER, conf.SMTP_PORT)
    s.ehlo()
    s.starttls()
    s.ehlo
    s.login(conf.SMTP_USER, conf.SMTP_PASSWORD)
    s.sendmail(conf.SMTP_USER, conf.DEST_EMAILS, msg.as_string())
    s.quit()

def safe_index(l, i=0):
    """Handle base exceptions for access to the list 
    """
    try:
        return l[i]
    except (IndexError, TypeError):
        return ''
        
def my_import(name):
    """Dynamic module import
    
    Args:
        name: string module path
    Returns:
        module object    
    """
    mod = __import__(name)
    components = name.split('.')
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod
    
def search_gtin(session, title):
    res = session.execute('select GTIN_CD, match(GTIN_NM) against("%s") from gtin where match(GTIN_NM) against("%s") limit 1;'\
         % (title, title)).fetchone()
    if res and res[1] > 20:
        return res[0]

    
    
